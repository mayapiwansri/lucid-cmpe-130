#network of people, like a directory
class SocialNetwork:
    def __init__(self):
        self.directory = {}

    def sign_up(self, node, password):
        self.directory[node.id] = {"Password": password, "node": node}

#a person
class Node:
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.friends = []
        self.messages = []
        self.visited = 0

    def add_friend(self, node):
        self.friends.append(node)

    def is_friend(self, node):
        return node in self.friends

    def add_post(self, node, message):
        if node in self.friends:
            # print("entered")
            node.messages.append((message, self))
        elif node is self:
            node.messages.append((message, self))
            # print("entered")


    def print_messages(self):
        for msg, node in self.messages[::-1]:
            print ("{}: {}".format(node.name, msg))


fb = SocialNetwork()
joe = Node(1, "joe")
tom = Node(2, "tom")
nancy = Node(3, "nancy")
lisa = Node(4, "lisa")
julie = Node(5, "julie")
mary = Node(6, "mary")
mike = Node(7, "mike")
ralph = Node(8, "ralph")
jack = Node(9, "jack")
charles = Node(10, "charles")
allan = Node(11, "allan")

fb.sign_up(joe, "joe")
fb.sign_up(tom, "tom")
fb.sign_up(nancy, "nancy")
fb.sign_up(lisa, "lisa")
fb.sign_up(julie, "julie")
fb.sign_up(mary, "mary")
fb.sign_up(mike, "mike")
fb.sign_up(ralph, "ralph")
fb.sign_up(jack, "jack")
fb.sign_up(charles, "charles")
fb.sign_up(allan, "allan")

joe.add_friend(tom)
tom.add_friend(joe)
tom.add_friend(lisa)
lisa.add_friend(tom)
lisa.add_friend(julie)
tom.add_friend(mary)
mary.add_friend(mike)
mike.add_friend(mary)
mary.add_friend(ralph)
ralph.add_friend(mary)
joe.add_friend(nancy)
nancy.add_friend(tom)
nancy.add_friend(joe)
# joe.add_friend(allan)
allan.add_friend(joe)

allan.add_post(joe, "hey joe its allan")
nancy.add_post(joe, "hey joe its nancy")
tom.add_post(joe, "hey joe its tom")





def BFS(vik, arv):
    # for key, v in fb.directory.items():
    #     v.get("node").visited = 0


    current = vik;
    endsearch = False
    found = False
    q = []
    depth = 0

    while 1:
        
        for node in current.friends:
            if node.visited is 0:  
                node.visited = 1
                q.append((node, depth+1))

        if(len(q) is 0):
            endsearch = True
            break
        else:
            t = q.pop(0)
            current = t[0]
            depth = t[1]

            if current is arv:
                print(current.name)
                found = True
                break

    if found is True:
        for key, v in fb.directory.items():
            v.get("node").visited = 0

        # for key,v in fb.directory.items():
        #     print(v.get("node").visited)

        return depth
    else:
        for key, v in fb.directory.items():
            v.get("node").visited = 0

        # for key,v in fb.directory.items():
        #     print(v.get("node").visited)

        print("Nodes are Not Connected")
        return 0



# a = BFS(v, arv)
# print(a)




from flask import Flask, request, render_template #import main Flask class and request object

app = Flask(__name__) #create the Flask app

@app.route('/form-example', methods=['GET', 'POST']) #allow both GET and POST requests
def form_example():
    if request.method == "GET": #this block is only entered when the form is submitted
        params = ""
        return render_template('index.html', params=params)
    
@app.route('/submit', methods=['GET', 'POST']) #allow both GET and POST requests
def submit():
    if request.method == 'POST': #this block is only entered when the form is submitted
        if request.form['Name'] is None:
            print("error")

        else:
            framework = request.form['Name']
            print(framework)
            for key in fb.directory.values():
                if key.get('node').name == framework:
                    joe.add_friend(key.get('node'))
                    break

                # print(key.get('node').name)
                # if node.name == framework:
                #     joe.add_friend(node)
                #     break
            # a.add_post(a, "helloooo")
            # a.add_post(a, "added a new post!!!")
            for i in joe.friends:
                print(i.name)
                i.print_messages()
                # print(i.name)

            
        return render_template('index.html', params = framework)

@app.route('/msg', methods=['GET', 'POST']) #allow both GET and POST requests
def msg():
    if request.method == 'POST': #this block is only entered when the form is submitted
        friend = request.form['friend']
        message = request.form['message']
        print(friend, message)
        for key in fb.directory.values():
                if key.get('node').name == friend:
                    joe.add_post(key.get('node'), message)
                    break
        return render_template('index.html', params = friend)



@app.route('/disp', methods=['GET', 'POST']) #allow both GET and POST requests
def disp():
    lst = []
    str1 = ""
    if request.method == 'POST': #this block is only entered when the form is submitted
        nd = request.form['dis']
        # print(nd)
        for key in fb.directory.values():
                if key.get('node').name == nd:
                    for msg, node in key.get('node').messages[::-1]:
                        lst.append("{}: {}".format(node.name, msg))
                        str1 = str1 + "{}: {}".format(node.name, msg) + "\n"
                    break
        print(str1)
        return render_template('index.html', param = lst)


@app.route('/link', methods=['GET', 'POST']) #allow both GET and POST requests
def link():
    v = 0
    a = 0
    num = 0
    str2 = ""

    if request.method == 'POST': #this block is only entered when the form is submitted
        friend1 = request.form['lin1']
        friend2 = request.form['lin2']
        # print(nd)
        for key in fb.directory.values():
            if key.get('node').name == friend1:
                v = key.get('node')
                break
        for key in fb.directory.values():
            if key.get('node').name == friend2:
                a = key.get('node')
                break

        num = BFS(v, a)
        str2 = v.name + " is " + str(num) + " connections away from " + a.name

        
        print(str2)
        return render_template('index.html', p2 = str2)



if __name__ == '__main__':
    app.run(debug=True) #run app in debug mode on port 5000
